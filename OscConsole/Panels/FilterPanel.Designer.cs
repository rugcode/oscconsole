﻿namespace OscConsole.Panels
{
	partial class FilterPanel
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.button1 = new System.Windows.Forms.Button();
			this.m_NamespaceAddress = new System.Windows.Forms.TextBox();
			this.m_DataView = new System.Windows.Forms.DataGridView();
			this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.Color = new OscConsole.Controls.ColorPickerColumn();
			this.Count = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Average = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Monitor = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colorPickerColumn1 = new OscConsole.Controls.ColorPickerColumn();
			this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			((System.ComponentModel.ISupportInitialize)(this.m_DataView)).BeginInit();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(510, 397);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(68, 23);
			this.button1.TabIndex = 8;
			this.button1.Text = "Add Filter";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.FilterButton_Click);
			// 
			// m_NamespaceAddress
			// 
			this.m_NamespaceAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_NamespaceAddress.Location = new System.Drawing.Point(0, 399);
			this.m_NamespaceAddress.Name = "m_NamespaceAddress";
			this.m_NamespaceAddress.Size = new System.Drawing.Size(504, 20);
			this.m_NamespaceAddress.TabIndex = 7;
			this.m_NamespaceAddress.TextChanged += new System.EventHandler(this.NamespaceAddress_TextChanged);
			this.m_NamespaceAddress.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.NamespaceAddress_PreviewKeyDown);
			// 
			// m_DataView
			// 
			this.m_DataView.AllowUserToAddRows = false;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.m_DataView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
			this.m_DataView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_DataView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.m_DataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.m_DataView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Address,
            this.Color,
            this.Count,
            this.Average,
            this.Monitor});
			this.m_DataView.Location = new System.Drawing.Point(0, 0);
			this.m_DataView.Name = "m_DataView";
			this.m_DataView.RowTemplate.ReadOnly = true;
			this.m_DataView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.m_DataView.Size = new System.Drawing.Size(578, 393);
			this.m_DataView.TabIndex = 6;
			this.m_DataView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataView_CellContentClick);
			this.m_DataView.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.DataView_UserDeletingRow);
			// 
			// Address
			// 
			this.Address.ContextMenuStrip = this.contextMenuStrip1;
			this.Address.DataPropertyName = "Address";
			this.Address.FillWeight = 19.35645F;
			this.Address.HeaderText = "Address";
			this.Address.Name = "Address";
			this.Address.ReadOnly = true;
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem,
            this.toolStripSeparator1});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(108, 32);
			// 
			// deleteToolStripMenuItem
			// 
			this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
			this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
			this.deleteToolStripMenuItem.Text = "Delete";
			this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
			// 
			// Color
			// 
			this.Color.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.Color.DataPropertyName = "Color";
			this.Color.FillWeight = 19.35645F;
			this.Color.HeaderText = "Color";
			this.Color.Name = "Color";
			this.Color.Width = 60;
			// 
			// Count
			// 
			this.Count.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.Count.DataPropertyName = "Count";
			this.Count.HeaderText = "Count";
			this.Count.MinimumWidth = 80;
			this.Count.Name = "Count";
			this.Count.ReadOnly = true;
			this.Count.Width = 80;
			// 
			// Average
			// 
			this.Average.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.Average.DataPropertyName = "Average";
			this.Average.FillWeight = 259.3763F;
			this.Average.HeaderText = "Av.";
			this.Average.Name = "Average";
			this.Average.ReadOnly = true;
			this.Average.Width = 67;
			// 
			// Monitor
			// 
			this.Monitor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.Monitor.DataPropertyName = "Monitor";
			this.Monitor.HeaderText = "";
			this.Monitor.MinimumWidth = 40;
			this.Monitor.Name = "Monitor";
			this.Monitor.Width = 40;
			// 
			// dataGridViewTextBoxColumn1
			// 
			this.dataGridViewTextBoxColumn1.ContextMenuStrip = this.contextMenuStrip1;
			this.dataGridViewTextBoxColumn1.DataPropertyName = "Address";
			this.dataGridViewTextBoxColumn1.FillWeight = 19.35645F;
			this.dataGridViewTextBoxColumn1.HeaderText = "Address";
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			this.dataGridViewTextBoxColumn1.Width = 288;
			// 
			// colorPickerColumn1
			// 
			this.colorPickerColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.colorPickerColumn1.DataPropertyName = "Color";
			this.colorPickerColumn1.FillWeight = 19.35645F;
			this.colorPickerColumn1.HeaderText = "Color";
			this.colorPickerColumn1.Name = "colorPickerColumn1";
			this.colorPickerColumn1.Width = 60;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.dataGridViewTextBoxColumn2.DataPropertyName = "Count";
			this.dataGridViewTextBoxColumn2.HeaderText = "Count";
			this.dataGridViewTextBoxColumn2.MinimumWidth = 80;
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			this.dataGridViewTextBoxColumn2.Width = 80;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.dataGridViewTextBoxColumn3.DataPropertyName = "Average";
			this.dataGridViewTextBoxColumn3.FillWeight = 259.3763F;
			this.dataGridViewTextBoxColumn3.HeaderText = "Av.";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			this.dataGridViewTextBoxColumn3.Width = 67;
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(104, 6);
			// 
			// FilterPanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.button1);
			this.Controls.Add(this.m_NamespaceAddress);
			this.Controls.Add(this.m_DataView);
			this.Name = "FilterPanel";
			this.Size = new System.Drawing.Size(578, 420);
			this.Load += new System.EventHandler(this.FilterPanel_Load);
			((System.ComponentModel.ISupportInitialize)(this.m_DataView)).EndInit();
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox m_NamespaceAddress;
		private System.Windows.Forms.DataGridView m_DataView;
		private System.Windows.Forms.DataGridViewTextBoxColumn Address;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
		private OscConsole.Controls.ColorPickerColumn Color;
		private System.Windows.Forms.DataGridViewTextBoxColumn Count;
		private System.Windows.Forms.DataGridViewTextBoxColumn Average;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Monitor;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
		private OscConsole.Controls.ColorPickerColumn colorPickerColumn1;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
	}
}
