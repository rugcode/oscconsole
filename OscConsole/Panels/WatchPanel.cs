﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using OscConsole.Core;

namespace OscConsole.Panels
{
	public partial class WatchPanel : UserControl, IMessageConsumer
	{
		private Dictionary<NamespaceInstance, DataGridViewRow> m_RowLookup = new Dictionary<NamespaceInstance,DataGridViewRow>(); 

		public event EventHandler SaveNeeded; 
			
		public WatchPanel()
		{
			InitializeComponent();
		}

		private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
		{
			NamespaceInstance instance = StaticObjects.NamespaceLookup[e.Row.Cells["Address"].Value.ToString()];

			Detach_Inner(instance); 
		}

		#region IMessageConsumer Members

		public void Attach(NamespaceInstance instance)
		{
			if (instance.Consumers.Contains(this) == false)
			{
				instance.Consumers.Add(this);
			}

			if (m_RowLookup.ContainsKey(instance) == false)
			{
				DataGridViewRow row = dataGridView1.Rows[dataGridView1.Rows.Add(instance.Address, string.Empty)];

				m_RowLookup.Add(instance, row); 
			}

			if (SaveNeeded != null)
			{
				SaveNeeded(this, EventArgs.Empty);
			}
		}

		public void Detach(NamespaceInstance instance)
		{			
			if (m_RowLookup.ContainsKey(instance) == true)
			{
				dataGridView1.Rows.Remove(m_RowLookup[instance]);

				Detach_Inner(instance);
			}
		}

		private void Detach_Inner(NamespaceInstance instance)
		{
			if (instance.Consumers.Contains(this) == true)
			{
				instance.Consumers.Remove(this);
			}

			if (m_RowLookup.ContainsKey(instance) == true)
			{
				m_RowLookup.Remove(instance);
			}

			if (SaveNeeded != null)
			{
				SaveNeeded(this, EventArgs.Empty);
			}
		}

		public void OnMessage(NamespaceInstance instance, Rug.Osc.OscMessage message)
		{
			if (m_RowLookup.ContainsKey(instance) == true)
			{
				m_RowLookup[instance].SetValues(instance.Address, message.ToString()); 
			}
		}

		#endregion

		#region IMessageConsumer Members

		public void Save(System.Xml.XmlElement node, NamespaceInstance instance)
		{
			Helper.AppendAttributeAndValue(node, "type", this.GetType().FullName); 
		}

		public new void Load(System.Xml.XmlNode node, NamespaceInstance instance)
		{
			Attach(instance); 
		}

		#endregion
	}
}
