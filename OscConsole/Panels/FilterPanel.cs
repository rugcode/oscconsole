﻿using System;
using System.Windows.Forms;
using Rug.Cmd;
using Rug.Osc;
using System.Collections.Generic;
using OscConsole.Core;

namespace OscConsole.Panels
{
	public partial class FilterPanel : UserControl
	{
		private List<ToolStripMenuItem> m_ConsumerItems = new List<ToolStripMenuItem>();

		public event EventHandler SaveNeeded; 

		public FilterPanel()
		{
			InitializeComponent();
		}
		
		#region Load Event

		private void FilterPanel_Load(object sender, EventArgs e)
		{
			m_NamespaceAddress.BackColor = System.Drawing.Color.LightGreen;

			foreach (ToolStripMenuItem item in m_ConsumerItems)
			{
				item.Click -= new EventHandler(item_Click);				
				contextMenuStrip1.Items.Remove(item);
				item.Dispose();
			}
			
			m_ConsumerItems.Clear(); 

			foreach (KeyValuePair<string, IMessageConsumer> consumer in StaticObjects.Consumers)
			{
				ToolStripMenuItem item = new ToolStripMenuItem();

				item.Name = consumer.Key;
				item.Size = new System.Drawing.Size(107, 22);
				item.Text = consumer.Value.Name;
				item.Tag = consumer.Value; 
				item.Click += new EventHandler(item_Click);

				contextMenuStrip1.Items.Add(item); 

				m_ConsumerItems.Add(item); 
			}
		}

		#endregion

		#region Add Filter

		public void AddFilter(OscMessage message)
		{
			try
			{
				NamespaceInstance instance;
				if (StaticObjects.NamespaceLookup.TryGetValue(message.Address, out instance) == true)
				{
					instance.OnMessage(message);
					return;
				}

				AddFilter(message.Address);
			}
			catch (Exception ex)
			{
				// explicitly tell the user why the address failed to parse
				RC.WriteException(001, "Error parsing osc message", ex);
			}
		}

		public void AddFilter(string address)
		{
			try
			{
				NamespaceInstance instance;
				if (StaticObjects.NamespaceLookup.TryGetValue(address, out instance) == true)
				{
					return;
				}

				DataGridViewRow row = m_DataView.Rows[m_DataView.Rows.Add(address, ConsoleThemeColor.TitleText, 0, 0d, true)];

				instance = new NamespaceInstance(address, row);

				StaticObjects.NamespaceLookup.Add(address, instance);
				StaticObjects.Manager.Attach(address, instance.OnMessage);
			}
			catch (Exception ex)
			{
				// explicitly tell the user why the address failed to parse
				RC.WriteException(002, "Error parsing osc address", ex);
			}
		}

		#endregion

		#region Add Namespace Filters

		private void NamespaceAddress_TextChanged(object sender, EventArgs e)
		{
			// try to parse the osc address literal
			if (OscAddress.IsValidAddressPattern(m_NamespaceAddress.Text) == true)
			{
				// if it parsed ok then green 
				m_NamespaceAddress.BackColor = System.Drawing.Color.LightGreen;
			}
			else
			{
				// if there was a problem parsing the address then red (pink) 
				m_NamespaceAddress.BackColor = System.Drawing.Color.Pink;
			}
		}

		private void NamespaceAddress_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
			{
				button1.PerformClick();
			}
		}

		private void FilterButton_Click(object sender, EventArgs e)
		{
			AddFilter(m_NamespaceAddress.Text);
		}

		#endregion 

		#region Data Grid Events

		private void DataView_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex < 0)
			{
				return;
			}

			string address = m_DataView.Rows[e.RowIndex].Cells["Address"].Value.ToString();

			NamespaceInstance instance = StaticObjects.NamespaceLookup[address];

			if (m_DataView.Columns[e.ColumnIndex] == m_DataView.Columns["Monitor"])
			{
				instance.IsMonitoring = !instance.IsMonitoring;
			}
			else if (m_DataView.Columns[e.ColumnIndex] == m_DataView.Columns["Color"])
			{
				instance.ColorIndex = (instance.ColorIndex + 1) % StaticObjects.Colors.Length;
			}
		}

		private void DataView_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
		{
			try
			{
				string address = e.Row.Cells["Address"].Value.ToString();

				Delete(address);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void Delete(string address)
		{
			try
			{
				NamespaceInstance instance = StaticObjects.NamespaceLookup[address];

				foreach (IMessageConsumer consumer in instance.Consumers.ToArray())
				{
					consumer.Detach(instance);
				}

				StaticObjects.Manager.Detach(address, StaticObjects.NamespaceLookup[address].OnMessage);

				StaticObjects.NamespaceLookup.Remove(address);

				if (SaveNeeded != null)
				{
					SaveNeeded(this, EventArgs.Empty); 
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		#endregion

		#region Context Menu Events

		private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (m_DataView.SelectedRows.Count == 0)
			{
				e.Cancel = true;
			}
		}

		private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
		{
			foreach (DataGridViewRow row in m_DataView.SelectedRows)
			{
				string address = row.Cells["Address"].Value.ToString(); 

				Delete(address);

				m_DataView.Rows.Remove(row);
			}
		}

		void item_Click(object sender, EventArgs e)
		{
			IMessageConsumer consumer = (sender as ToolStripMenuItem).Tag as IMessageConsumer;

			foreach (DataGridViewRow row in m_DataView.SelectedRows)
			{
				consumer.Attach(StaticObjects.NamespaceLookup[row.Cells["Address"].Value.ToString()]);
			}
		}

		#endregion 
	
		internal void PopulateData(Dictionary<string, NamespaceInstance> dictionary)
		{			
			foreach (NamespaceInstance loadedInstance in dictionary.Values)
			{
				try
				{
					NamespaceInstance instance;
					if (StaticObjects.NamespaceLookup.TryGetValue(loadedInstance.Address, out instance) == false)
					{
						DataGridViewRow row = m_DataView.Rows[m_DataView.Rows.Add(loadedInstance.Address, ConsoleThemeColor.TitleText, 0, 0d, true)];

						instance = new NamespaceInstance(loadedInstance, row);	
					}					

					StaticObjects.NamespaceLookup.Add(instance.Address, instance);
					StaticObjects.Manager.Attach(instance.Address, instance.OnMessage);

					instance.LoadConsumers(loadedInstance); 
				}
				catch (Exception ex)
				{
					// explicitly tell the user why the address failed to parse
					RC.WriteException(002, "Error parsing osc address", ex);
				}				
			}
		}

		internal void Clear()
		{
			m_DataView.Rows.Clear();

			List<string> allAddresses = new List<string>(StaticObjects.NamespaceLookup.Keys);

			foreach (string address in allAddresses)
			{
				Delete(address);
			}
		}
	}
}
