﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using OscConsole.Core;
using OscConsole.Dialogs;
using OscConsole.Panels;
using Rug.Cmd;
using Rug.Cmd.Colors;
using Rug.Osc; 

namespace OscConsole
{
    public partial class MainForm : Form, IOscConsoleHost
    {
		delegate void StringEvent1(string str);
		delegate void StringEvent2(ConsoleThemeColor color, string str);
		delegate void StringEvent3(ConsoleVerbosity verbose, ConsoleThemeColor color, string str);
		delegate void ExceptionEvent(Exception ex);

		delegate void OscPacketEvent(OscPacket packet);

		private const string m_BaseTitle = "OscConsole";

		private Options m_Options = new Options();
		private ConsoleSetup m_Setup;

        OscReceiver m_Receiver;		

        Thread m_Thread;
		private DateTime m_LastTime;

		bool m_ChangesHaveBeenMadeSinceLastSave = false; 
				
        public MainForm()
        {
            RC.Verbosity = ConsoleVerbosity.Normal;

            InitializeComponent();

			filterPanel1.SaveNeeded += new EventHandler(OnSaveNeeded);
			Watch.SaveNeeded += new EventHandler(OnSaveNeeded);

			StaticObjects.Consumers.Add(typeof(WatchPanel).FullName, Watch); 
        }

		void OnSaveNeeded(object sender, EventArgs e)
		{
			m_ChangesHaveBeenMadeSinceLastSave = true;
		}

        private void MainForm_Load(object sender, EventArgs e)
        {
			FormWindowState state = m_Options.WindowState; 
			
			if (m_Options.Bounds != Rectangle.Empty)
			{
				this.DesktopBounds = m_Options.Bounds;
			}

			WindowState = state;

			RC.App = consoleControl1.Buffer;			
			RC.BackgroundColor = ConsoleColorExt.Black;
			RC.Theme = ConsoleColorTheme.Load(ConsoleColorDefaultThemes.Colorful);
			consoleControl1.SetColors();

			LoadXml(m_Options.ConsoleSetupFilePath);

			SetWindowTitle();

			connectToolStripMenuItem_Click(this, EventArgs.Empty); 
		}

		#region File Handlers

		#region Set Window Title

		private void SetWindowTitle()
		{
			if (m_Options.ConsoleSetupFilePath == null)
			{
				this.Text = "Unsaved - " + m_BaseTitle;
			}
			else
			{
				this.Text = m_Options.ConsoleSetupFilePath + " - " + m_BaseTitle;
			}
		}

		#endregion

		#region New File

		private void NewFile()
		{
			m_ChangesHaveBeenMadeSinceLastSave = true; 

			disconnectToolStripMenuItem_Click(null, EventArgs.Empty); 

			m_Options.ConsoleSetupFilePath = null;

			m_Setup = new ConsoleSetup(null);

			filterPanel1.Clear(); 

			SetWindowTitle();
		}

		#endregion

		#region Open

		private void OpenFile()
		{
			if (m_OpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				disconnectToolStripMenuItem_Click(this, EventArgs.Empty); 

				m_Options.ConsoleSetupFilePath = m_OpenFileDialog.FileName;

				if (LoadXml(m_Options.ConsoleSetupFilePath) == false)
				{
					m_Options.ConsoleSetupFilePath = null;
				}

				SetWindowTitle();

				connectToolStripMenuItem_Click(this, EventArgs.Empty); 
			}
		}

		#endregion

		#region Save File

		private void SaveFile()
		{
			if (m_Options.ConsoleSetupFilePath == null)
			{
				SaveFileAs();
			}
			else
			{
				SaveXml(m_Options.ConsoleSetupFilePath);
			}
		}

		#endregion

		#region Save File As

		private void SaveFileAs()
		{
			if (m_SaveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				m_Options.ConsoleSetupFilePath = m_SaveFileDialog.FileName;

				SaveXml(m_Options.ConsoleSetupFilePath);

				SetWindowTitle();
			}
		}

		#endregion

		#region Exit

		private void Exit()
		{
			Close();
		}

		#endregion

		#endregion


		#region Xml Handlers

		#region Load Xml

		private bool LoadXml(string xmlPath)
		{
			filterPanel1.Clear();

			m_Setup = new ConsoleSetup(xmlPath);

			PopulateData();

			m_ChangesHaveBeenMadeSinceLastSave = false;			

			return true;
		}

		void PopulateData()
		{
			filterPanel1.PopulateData(m_Setup.Namespaces);
		}

		#endregion

		#region Save Xml

		private void SaveXml(string xmlPath)
		{
			m_Setup.FilePath = xmlPath; 

			m_Setup.Namespaces.Clear();

			foreach (NamespaceInstance instance in StaticObjects.NamespaceLookup.Values)
			{
				m_Setup.Namespaces.Add(instance.Address, instance);
			}

			m_Setup.Save();

			m_ChangesHaveBeenMadeSinceLastSave = false;
		}

		#endregion

		#endregion


        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (ConnectDialog dialog = new ConnectDialog())
            {
				dialog.Address = m_Setup.Address;
				dialog.Port = m_Setup.Port;    

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
					m_Setup.Address = dialog.Address;
					m_Setup.Port = dialog.Port;

					m_ChangesHaveBeenMadeSinceLastSave = false;
                }
            }
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            disconnectToolStripMenuItem_Click(sender, e);

            // create the reciever instance
			m_Receiver = new OscReceiver(m_Setup.Address, m_Setup.Port);

            // tell the user
			RC.WriteLine(ConsoleVerbosity.Silent, ConsoleThemeColor.TextGood, String.Format("Listening on: {0}:{1}", m_Setup.Address, m_Setup.Port));

            try
            {
                // connect to the socket 
                m_Receiver.Connect();
            }
            catch (Exception ex)
            {
				//this.Invoke(new StringEvent2(RC.WriteLine), ConsoleThemeColor.TextBad, "Exception while connecting");
				//this.Invoke(new StringEvent2(RC.WriteLine), ConsoleThemeColor.SubTextBad, ex.Message);

				RC.WriteLine(ConsoleThemeColor.TextBad, "Exception while connecting");
				RC.WriteLine(ConsoleThemeColor.SubTextBad, ex.Message);

                m_Receiver.Dispose();
                m_Receiver = null;

                return;
            }

            // create the listen thread
            m_Thread = new Thread(ListenLoop);

            // start listening
            m_Thread.Start();

			m_LastTime = DateTime.Now;

			timer1.Enabled = true; 
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
			timer1.Enabled = false;
 
            // if there is already an instace dispose of it
            if (m_Receiver != null)
            {
                // dispose of the reciever
				RC.WriteLine(ConsoleVerbosity.Silent, ConsoleThemeColor.TextGood, "Disconnecting");
                
				m_Receiver.Dispose();

				while (m_Thread != null &&
					m_Thread.IsAlive == true)
				{
					// wait for the thread to exit (IMPORTANT!) 
					m_Thread.Join(10);

					Application.DoEvents(); 
				}

				m_Thread = null; 
                m_Receiver = null;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            disconnectToolStripMenuItem_Click(sender, e);

			if (e.CloseReason == CloseReason.TaskManagerClosing) 
			{
				return; 
			}

			if (m_ChangesHaveBeenMadeSinceLastSave == true)
			{
				DialogResult result = MessageBox.Show("There are unsaved changes. Do you want to save them?", "Save changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

				switch (result)
				{
					case DialogResult.Cancel:
						e.Cancel = true; 
						break;
					case DialogResult.No:
						break;
					case DialogResult.Yes:
						SaveFile();
						break;
					default:
						break;
				}				
			}	

			m_Options.Save(); 
        }


        void ListenLoop()
        {
            try
            {
                while (m_Receiver.State != OscSocketState.Closed)
                {
                    // if we are in a state to recieve
                    if (m_Receiver.State == OscSocketState.Connected)
                    {
                        // get the next message 
                        // this will block until one arrives or the socket is closed
                        OscPacket packet = m_Receiver.Receive();

                        if (packet.Error == OscPacketError.None)
                        {
							PacketRecived(packet); // this.Invoke(new OscPacketEvent(PacketRecived), packet);							
                        }
                        else
                        {
							this.Invoke(new StringEvent2(RC.WriteLine), ConsoleThemeColor.TextBad, "Error reading packet, " + packet.Error);
							this.Invoke(new StringEvent2(RC.WriteLine), ConsoleThemeColor.SubTextBad, packet.ErrorMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // if the socket was connected when this happens
                // then tell the user
                if (m_Receiver.State == OscSocketState.Connected)
                {
					this.Invoke(new StringEvent2(RC.WriteLine), ConsoleThemeColor.TextBad, "Exception in listen loop");
					this.Invoke(new StringEvent2(RC.WriteLine), ConsoleThemeColor.SubTextBad, ex.Message);
                }
            }
        }

		private void PacketRecived(OscPacket packet)
		{
			if (StaticObjects.Manager.Invoke(packet) == false)
			{
				if (packet is OscMessage)
				{
					//this.Invoke(new StringEvent2(RC.WriteLine), ConsoleThemeColor.SubTextNutral, packet.ToString());
					RC.WriteLine(ConsoleThemeColor.SubTextNutral, packet.ToString());

					this.Invoke(new OscMessageEvent(filterPanel1.AddFilter), (packet as OscMessage));
					//AddEvent((packet as OscMessage).Address);
				}
			}
		}


		#region Menu Events

		private void newToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NewFile();
		}

		private void openToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OpenFile();
		}

		private void saveToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveFile();
		}

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveFileAs();
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Exit();
		}

		#endregion 
		

		#region Window Resize / Move Events

		private void MainForm_ResizeBegin(object sender, EventArgs e)
		{

		}

		private void MainForm_ResizeEnd(object sender, EventArgs e)
		{
			if (WindowState == FormWindowState.Normal)
			{
				m_Options.Bounds = this.DesktopBounds;
			}
		}

		private void MainForm_Resize(object sender, EventArgs e)
		{
			if (WindowState != FormWindowState.Minimized)
			{
				m_Options.WindowState = WindowState;
			}
		}

		#endregion

		private void timer1_Tick(object sender, EventArgs e)
		{
			DateTime now = DateTime.Now;

			TimeSpan span = now.Subtract(m_LastTime);

			foreach (NamespaceInstance instance in StaticObjects.NamespaceLookup.Values)
			{
				instance.CalculateAverage(span); 
			}

			m_LastTime = now; 
		}
    }
}

/*
using System;

using System.Collections.Generic;

using System.ComponentModel;
 * 

using System.Data;

using System.Drawing;

using System.Text;

using System.Windows.Forms;

 

namespace WindowsApplication1

{

    public partial class DgvComboColor : Form

    {

        public DgvComboColor()

        {

            InitializeComponent();

        }

 

        private ComboBox cmb;

        private void DgvComboColor_Load(object sender, EventArgs e)

        {

            DataTable dt = new DataTable();

            //columns:

            dt.Columns.Add("id",typeof(int));

            dt.Columns.Add("name");

            dt.Columns.Add("Color", typeof(Color));

            

            //data:

            dt.Rows.Add(1, "name1", Color.Red);

            dt.Rows.Add(2, "name1", Color.Blue);

            dt.Rows.Add(3, "name1", Color.Red);

            dt.Rows.Add(4, "name1", Color.Yellow);

            dt.Rows.Add(5, "name1", Color.Red);

            dt.Rows.Add(6, "name1", Color.Gray);

 

            this.dataGridView1.DataSource = dt;

 

            //Combobox

            cmb = new ComboBox();

            cmb.Items.Add(Color.Gray);

            cmb.Items.Add(Color.Green);

            cmb.Items.Add(Color.Red);

            cmb.Items.Add(Color.Blue);

            cmb.Items.Add(Color.Yellow);

            cmb.Items.Add(Color.White);

            cmb.Items.Add(Color.Black);

            cmb.Items.Add(Color.CornflowerBlue);

            cmb.Items.Add(Color.Firebrick);

            cmb.DrawMode = DrawMode.OwnerDrawFixed;

            cmb.DrawItem +=new DrawItemEventHandler(cmb_DrawItem);

            cmb.Visible = false;

 

            this.dataGridView1.Controls.Add(cmb);

        }

 

        void cmb_DrawItem(object sender, DrawItemEventArgs e)

        {

            e.DrawBackground();

            ComboBox cmb = (ComboBox)sender;

            Color color = (Color)cmb.Items[e.Index];

            e.Graphics.FillRectangle(new SolidBrush(color), e.Bounds);

            e.Graphics.DrawString(color.Name, e.Font, new SolidBrush(e.ForeColor), e.Bounds);

        }

 

        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)

        {

            if (e.ColumnIndex == this.dataGridView1.Columns["Color"].Index)

            {

                Rectangle ret = this.dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);

                this.cmb.Location = ret.Location;

                this.cmb.SelectedItem = (Color)this.dataGridView1.CurrentCell.Value;

                this.cmb.Visible = true;

            }

            else

            {

                this.cmb.Visible = false;

            }

        }

 

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)

        {

            this.dataGridView1.CurrentCell.Value = this.cmb.SelectedItem;

            this.cmb.Visible = false;

        }

 

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)

        {

            if (e.ColumnIndex == this.dataGridView1.Columns["Color"].Index

                && e.RowIndex != this.dataGridView1.NewRowIndex)

            {

                e.CellStyle.BackColor = (Color)e.Value;

            }

        }

    }

}
*/ 