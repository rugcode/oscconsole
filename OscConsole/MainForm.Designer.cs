﻿namespace OscConsole
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.consoleControl1 = new Rug.Cmd.Forms.ConsoleControl();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.consolePage = new System.Windows.Forms.TabPage();
			this.namespacesPage = new System.Windows.Forms.TabPage();
			this.watchPage = new System.Windows.Forms.TabPage();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.connectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.disconnectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.m_OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.m_SaveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.pluginsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.consoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.namespacesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.filterPanel1 = new OscConsole.Panels.FilterPanel();
			this.Watch = new OscConsole.Panels.WatchPanel();
			this.colorPickerColumn1 = new OscConsole.Controls.ColorPickerColumn();
			this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colorPickerColumn2 = new OscConsole.Controls.ColorPickerColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tabControl1.SuspendLayout();
			this.consolePage.SuspendLayout();
			this.namespacesPage.SuspendLayout();
			this.watchPage.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// consoleControl1
			// 
			this.consoleControl1.BackColor = System.Drawing.Color.Black;
			this.consoleControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.consoleControl1.Font = new System.Drawing.Font("Courier New", 12F);
			this.consoleControl1.Location = new System.Drawing.Point(3, 3);
			this.consoleControl1.Name = "consoleControl1";
			this.consoleControl1.ReadOnly = true;
			this.consoleControl1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
			this.consoleControl1.Size = new System.Drawing.Size(670, 326);
			this.consoleControl1.TabIndex = 1;
			this.consoleControl1.Text = "";
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.consolePage);
			this.tabControl1.Controls.Add(this.namespacesPage);
			this.tabControl1.Controls.Add(this.watchPage);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 24);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(684, 358);
			this.tabControl1.TabIndex = 2;
			// 
			// consolePage
			// 
			this.consolePage.Controls.Add(this.consoleControl1);
			this.consolePage.Location = new System.Drawing.Point(4, 22);
			this.consolePage.Name = "consolePage";
			this.consolePage.Padding = new System.Windows.Forms.Padding(3);
			this.consolePage.Size = new System.Drawing.Size(676, 332);
			this.consolePage.TabIndex = 0;
			this.consolePage.Text = "Console";
			this.consolePage.UseVisualStyleBackColor = true;
			// 
			// namespacesPage
			// 
			this.namespacesPage.Controls.Add(this.filterPanel1);
			this.namespacesPage.Location = new System.Drawing.Point(4, 22);
			this.namespacesPage.Name = "namespacesPage";
			this.namespacesPage.Padding = new System.Windows.Forms.Padding(3);
			this.namespacesPage.Size = new System.Drawing.Size(453, 339);
			this.namespacesPage.TabIndex = 1;
			this.namespacesPage.Text = "Namespaces";
			this.namespacesPage.UseVisualStyleBackColor = true;
			// 
			// watchPage
			// 
			this.watchPage.Controls.Add(this.Watch);
			this.watchPage.Location = new System.Drawing.Point(4, 22);
			this.watchPage.Name = "watchPage";
			this.watchPage.Padding = new System.Windows.Forms.Padding(3);
			this.watchPage.Size = new System.Drawing.Size(453, 339);
			this.watchPage.TabIndex = 2;
			this.watchPage.Text = "Watch";
			this.watchPage.UseVisualStyleBackColor = true;
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.connectionsToolStripMenuItem,
            this.windowsToolStripMenuItem,
            this.pluginsToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(684, 24);
			this.menuStrip1.TabIndex = 3;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.newToolStripMenuItem,
            this.toolStripSeparator2,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.openToolStripMenuItem.Text = "&Open";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
			this.newToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.newToolStripMenuItem.Text = "&New";
			this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(181, 6);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.saveToolStripMenuItem.Text = "&Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.saveAsToolStripMenuItem.Text = "&Save as";
			this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(181, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.exitToolStripMenuItem.Text = "&Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// connectionsToolStripMenuItem
			// 
			this.connectionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.connectToolStripMenuItem,
            this.disconnectToolStripMenuItem});
			this.connectionsToolStripMenuItem.Name = "connectionsToolStripMenuItem";
			this.connectionsToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
			this.connectionsToolStripMenuItem.Text = "Connection";
			// 
			// settingsToolStripMenuItem
			// 
			this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
			this.settingsToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
			this.settingsToolStripMenuItem.Text = "Settings";
			this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
			// 
			// connectToolStripMenuItem
			// 
			this.connectToolStripMenuItem.Name = "connectToolStripMenuItem";
			this.connectToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
			this.connectToolStripMenuItem.Text = "Connect";
			this.connectToolStripMenuItem.Click += new System.EventHandler(this.connectToolStripMenuItem_Click);
			// 
			// disconnectToolStripMenuItem
			// 
			this.disconnectToolStripMenuItem.Name = "disconnectToolStripMenuItem";
			this.disconnectToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
			this.disconnectToolStripMenuItem.Text = "Disconnect";
			this.disconnectToolStripMenuItem.Click += new System.EventHandler(this.disconnectToolStripMenuItem_Click);
			// 
			// timer1
			// 
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// m_OpenFileDialog
			// 
			this.m_OpenFileDialog.Filter = "Xml files|*.xml|All files|*.*";
			this.m_OpenFileDialog.Title = "Open Console Setup";
			// 
			// m_SaveFileDialog
			// 
			this.m_SaveFileDialog.Filter = "Xml files|*.xml|All files|*.*";
			this.m_SaveFileDialog.Title = "Save Console Setup";
			// 
			// pluginsToolStripMenuItem
			// 
			this.pluginsToolStripMenuItem.Name = "pluginsToolStripMenuItem";
			this.pluginsToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
			this.pluginsToolStripMenuItem.Text = "Plugins";
			// 
			// windowsToolStripMenuItem
			// 
			this.windowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consoleToolStripMenuItem,
            this.namespacesToolStripMenuItem,
            this.toolStripSeparator3});
			this.windowsToolStripMenuItem.Name = "windowsToolStripMenuItem";
			this.windowsToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.windowsToolStripMenuItem.Text = "View";
			// 
			// consoleToolStripMenuItem
			// 
			this.consoleToolStripMenuItem.Name = "consoleToolStripMenuItem";
			this.consoleToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
			this.consoleToolStripMenuItem.Text = "Console";
			// 
			// namespacesToolStripMenuItem
			// 
			this.namespacesToolStripMenuItem.Name = "namespacesToolStripMenuItem";
			this.namespacesToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
			this.namespacesToolStripMenuItem.Text = "Namespaces";
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(138, 6);
			// 
			// filterPanel1
			// 
			this.filterPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.filterPanel1.Location = new System.Drawing.Point(3, 3);
			this.filterPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.filterPanel1.Name = "filterPanel1";
			this.filterPanel1.Size = new System.Drawing.Size(447, 333);
			this.filterPanel1.TabIndex = 0;
			// 
			// Watch
			// 
			this.Watch.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Watch.Location = new System.Drawing.Point(3, 3);
			this.Watch.Name = "Watch";
			this.Watch.Size = new System.Drawing.Size(447, 333);
			this.Watch.TabIndex = 4;
			// 
			// colorPickerColumn1
			// 
			this.colorPickerColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.colorPickerColumn1.DataPropertyName = "Color";
			this.colorPickerColumn1.FillWeight = 19.35645F;
			this.colorPickerColumn1.HeaderText = "Color";
			this.colorPickerColumn1.Name = "colorPickerColumn1";
			this.colorPickerColumn1.Width = 60;
			// 
			// dataGridViewTextBoxColumn1
			// 
			this.dataGridViewTextBoxColumn1.DataPropertyName = "MethodID";
			this.dataGridViewTextBoxColumn1.HeaderText = "ID";
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			this.dataGridViewTextBoxColumn1.Visible = false;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this.dataGridViewTextBoxColumn2.DataPropertyName = "Address";
			this.dataGridViewTextBoxColumn2.FillWeight = 19.35645F;
			this.dataGridViewTextBoxColumn2.HeaderText = "Address";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			this.dataGridViewTextBoxColumn2.Width = 312;
			// 
			// colorPickerColumn2
			// 
			this.colorPickerColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.colorPickerColumn2.DataPropertyName = "Color";
			this.colorPickerColumn2.FillWeight = 19.35645F;
			this.colorPickerColumn2.HeaderText = "Color";
			this.colorPickerColumn2.Name = "colorPickerColumn2";
			this.colorPickerColumn2.Width = 60;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.dataGridViewTextBoxColumn3.DataPropertyName = "Count";
			this.dataGridViewTextBoxColumn3.HeaderText = "Count";
			this.dataGridViewTextBoxColumn3.MinimumWidth = 80;
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			this.dataGridViewTextBoxColumn3.Width = 80;
			// 
			// dataGridViewTextBoxColumn4
			// 
			this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
			this.dataGridViewTextBoxColumn4.DataPropertyName = "Average";
			this.dataGridViewTextBoxColumn4.FillWeight = 259.3763F;
			this.dataGridViewTextBoxColumn4.HeaderText = "Av.";
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			this.dataGridViewTextBoxColumn4.ReadOnly = true;
			this.dataGridViewTextBoxColumn4.Width = 67;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(684, 382);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.MinimumSize = new System.Drawing.Size(700, 420);
			this.Name = "MainForm";
			this.Text = "Osc Console";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.ResizeBegin += new System.EventHandler(this.MainForm_ResizeBegin);
			this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			this.tabControl1.ResumeLayout(false);
			this.consolePage.ResumeLayout(false);
			this.namespacesPage.ResumeLayout(false);
			this.watchPage.ResumeLayout(false);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private Rug.Cmd.Forms.ConsoleControl consoleControl1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage consolePage;
        private System.Windows.Forms.TabPage namespacesPage;
		private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem connectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem disconnectToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
		private OscConsole.Controls.ColorPickerColumn colorPickerColumn1;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
		private System.Windows.Forms.TabPage watchPage;
		private OscConsole.Controls.ColorPickerColumn colorPickerColumn2;
		private System.Windows.Forms.Timer timer1;
		private OscConsole.Panels.FilterPanel filterPanel1;
		private OscConsole.Panels.WatchPanel Watch;
		private System.Windows.Forms.OpenFileDialog m_OpenFileDialog;
		private System.Windows.Forms.SaveFileDialog m_SaveFileDialog;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem windowsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem consoleToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem namespacesToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripMenuItem pluginsToolStripMenuItem;
    }
}

