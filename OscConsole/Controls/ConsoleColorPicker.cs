﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Rug.Cmd;
using Rug.Cmd.Forms;

namespace OscConsole.Controls
{
	public partial class ConsoleColorPicker : UserControl
	{
		List<Panel> colors = new List<Panel>();

		public ConsoleThemeColor Color { get; set; } 

		public ConsoleColorPicker()
		{
			InitializeComponent();
		}

		private void panel_MouseClick(object sender, MouseEventArgs e)
		{
			Color = (ConsoleThemeColor)(sender as Panel).Tag; 
		}

		private void ConsoleColorPicker_Load(object sender, EventArgs e)
		{
			colors.Clear();

			colors.Add(panel1);
			colors.Add(panel2);
			colors.Add(panel3);
			colors.Add(panel4);
			colors.Add(panel5);
			colors.Add(panel6);
			colors.Add(panel7);
			colors.Add(panel8);
			colors.Add(panel9);
			colors.Add(panel10);
			colors.Add(panel11);
			colors.Add(panel12);
			colors.Add(panel13);
			colors.Add(panel14);
			colors.Add(panel15);

			int index = 1; 
			foreach (Panel panel in colors)
			{
				ConsoleThemeColor color = (ConsoleThemeColor)index++; 

				panel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
				panel.BackColor = ConsoleControl.GetColor(RC.Theme[color]);
				panel.Tag = color; 
			}
		}
	}
}
