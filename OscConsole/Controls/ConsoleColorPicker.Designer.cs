﻿namespace OscConsole.Controls
{
	partial class ConsoleColorPicker
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.panel6 = new System.Windows.Forms.Panel();
			this.panel7 = new System.Windows.Forms.Panel();
			this.panel8 = new System.Windows.Forms.Panel();
			this.panel9 = new System.Windows.Forms.Panel();
			this.panel10 = new System.Windows.Forms.Panel();
			this.panel11 = new System.Windows.Forms.Panel();
			this.panel12 = new System.Windows.Forms.Panel();
			this.panel13 = new System.Windows.Forms.Panel();
			this.panel14 = new System.Windows.Forms.Panel();
			this.panel15 = new System.Windows.Forms.Panel();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Location = new System.Drawing.Point(3, 3);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(20, 20);
			this.panel1.TabIndex = 0;
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel2.Location = new System.Drawing.Point(29, 3);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(20, 20);
			this.panel2.TabIndex = 1;
			// 
			// panel3
			// 
			this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel3.Location = new System.Drawing.Point(55, 3);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(20, 20);
			this.panel3.TabIndex = 1;
			// 
			// panel4
			// 
			this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel4.Location = new System.Drawing.Point(81, 3);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(20, 20);
			this.panel4.TabIndex = 1;
			// 
			// panel5
			// 
			this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel5.Location = new System.Drawing.Point(107, 3);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(20, 20);
			this.panel5.TabIndex = 1;
			// 
			// panel6
			// 
			this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel6.Location = new System.Drawing.Point(107, 29);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(20, 20);
			this.panel6.TabIndex = 4;
			// 
			// panel7
			// 
			this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel7.Location = new System.Drawing.Point(81, 29);
			this.panel7.Name = "panel7";
			this.panel7.Size = new System.Drawing.Size(20, 20);
			this.panel7.TabIndex = 5;
			// 
			// panel8
			// 
			this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel8.Location = new System.Drawing.Point(55, 29);
			this.panel8.Name = "panel8";
			this.panel8.Size = new System.Drawing.Size(20, 20);
			this.panel8.TabIndex = 6;
			// 
			// panel9
			// 
			this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel9.Location = new System.Drawing.Point(29, 29);
			this.panel9.Name = "panel9";
			this.panel9.Size = new System.Drawing.Size(20, 20);
			this.panel9.TabIndex = 3;
			// 
			// panel10
			// 
			this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel10.Location = new System.Drawing.Point(3, 29);
			this.panel10.Name = "panel10";
			this.panel10.Size = new System.Drawing.Size(20, 20);
			this.panel10.TabIndex = 2;
			// 
			// panel11
			// 
			this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel11.Location = new System.Drawing.Point(107, 55);
			this.panel11.Name = "panel11";
			this.panel11.Size = new System.Drawing.Size(20, 20);
			this.panel11.TabIndex = 4;
			// 
			// panel12
			// 
			this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel12.Location = new System.Drawing.Point(81, 55);
			this.panel12.Name = "panel12";
			this.panel12.Size = new System.Drawing.Size(20, 20);
			this.panel12.TabIndex = 5;
			// 
			// panel13
			// 
			this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel13.Location = new System.Drawing.Point(55, 55);
			this.panel13.Name = "panel13";
			this.panel13.Size = new System.Drawing.Size(20, 20);
			this.panel13.TabIndex = 6;
			// 
			// panel14
			// 
			this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel14.Location = new System.Drawing.Point(29, 55);
			this.panel14.Name = "panel14";
			this.panel14.Size = new System.Drawing.Size(20, 20);
			this.panel14.TabIndex = 3;
			// 
			// panel15
			// 
			this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel15.Location = new System.Drawing.Point(3, 55);
			this.panel15.Name = "panel15";
			this.panel15.Size = new System.Drawing.Size(20, 20);
			this.panel15.TabIndex = 2;
			// 
			// ConsoleColorPicker
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panel11);
			this.Controls.Add(this.panel6);
			this.Controls.Add(this.panel12);
			this.Controls.Add(this.panel5);
			this.Controls.Add(this.panel13);
			this.Controls.Add(this.panel14);
			this.Controls.Add(this.panel7);
			this.Controls.Add(this.panel15);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel8);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel9);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel10);
			this.Controls.Add(this.panel1);
			this.Name = "ConsoleColorPicker";
			this.Size = new System.Drawing.Size(131, 79);
			this.Load += new System.EventHandler(this.ConsoleColorPicker_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Panel panel7;
		private System.Windows.Forms.Panel panel8;
		private System.Windows.Forms.Panel panel9;
		private System.Windows.Forms.Panel panel10;
		private System.Windows.Forms.Panel panel11;
		private System.Windows.Forms.Panel panel12;
		private System.Windows.Forms.Panel panel13;
		private System.Windows.Forms.Panel panel14;
		private System.Windows.Forms.Panel panel15;
	}
}
