﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace OscConsole.Dialogs
{
    public partial class ConnectDialog : Form
    {
        struct IntefaceInfo
        {
            public string String;
            public IPAddress IpAddress;

            public override string ToString()
            {
                return String;
            }
        }

        public IPAddress Address { get; set; }

        public int Port { get; set; } 


        public ConnectDialog()
        {
            InitializeComponent();
        }

        private void ConnectDialog_Load(object sender, EventArgs e)
        {            
            interfaceBox.Items.Add(new IntefaceInfo() { String = "Any", IpAddress = IPAddress.Any } );
			interfaceBox.Items.Add(new IntefaceInfo() { String = "Any IPv6", IpAddress = IPAddress.IPv6Any });

            // Get host name
            String strHostName = Dns.GetHostName();

            // Find host by name
            IPHostEntry iphostentry = Dns.GetHostEntry(strHostName);

            NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();

            foreach (NetworkInterface adapter in interfaces)
            {                
                var ipProps = adapter.GetIPProperties();

                foreach (var ip in ipProps.UnicastAddresses)
                {
                    if ((adapter.OperationalStatus == OperationalStatus.Up) && 
                        ((ip.Address.AddressFamily == AddressFamily.InterNetwork) ||
						(ip.Address.AddressFamily == AddressFamily.InterNetworkV6)))
                    {
                        interfaceBox.Items.Add(new IntefaceInfo() { String = adapter.Description.ToString() + " (" + ip.Address.ToString() + ")", IpAddress = ip.Address });
                    }
                }
            }

            //addressBox.Items.Add("127.0.0.1");
            //addressBox.Items.Add("Loopback IPv6");
            //addressBox.Items.Add("255.255.255.255");

            interfaceBox.SelectedIndex = 0; 

            for (int i = 0; i < interfaceBox.Items.Count; i++)
            {
                if (((IntefaceInfo)interfaceBox.SelectedItem).IpAddress == Address)
                {
                    interfaceBox.SelectedIndex = i;
                }
            }

            //addressBox.SelectedIndex = 1;
            
            portBox.Maximum = new decimal(new int[] { 65535, 0, 0, 0 });
            portBox.Value = (decimal)Port;
        }

        private void save_Click(object sender, EventArgs e)
        {
            Address = ((IntefaceInfo)interfaceBox.SelectedItem).IpAddress;
            Port = (int)portBox.Value; 

            Close(); 
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            Close(); 
        }


    }
}
