﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using OscConsole.Core;

namespace OscConsole
{
	class ConsoleSetup
	{
		public string FilePath { get; set; } 

		/// <summary>
		/// IP Address
		/// </summary>
		public IPAddress Address { get; set; }

		/// <summary>
		/// Port
		/// </summary>
		public int Port { get; set; }

		public readonly Dictionary<string, NamespaceInstance> Namespaces = new Dictionary<string, NamespaceInstance>(); 

		/// <summary>
		/// Load the options 
		/// </summary>
		public ConsoleSetup(string path)
		{
			FilePath = path; 

			// set all options to their defaults 
			SetDefaults();

			// check to see if there is a options file to load 
			if (Helper.IsNullOrEmpty(FilePath) ||
				File.Exists(Helper.ResolvePath(FilePath)) == false)
			{
				return;
			}

			// try to load the options
			Load();
		}

		private void SetDefaults()
		{
			// null the marking file
			Address = IPAddress.Any;

			// whatever
			Port = 5000;
		}

		private void Load()
		{
			try
			{
				XmlDocument doc = new XmlDocument();

				// load the options from the resolved path
				doc.Load(Helper.ResolvePath(FilePath));

				XmlNode node = doc.DocumentElement;

				// if the node is not null 
				if (node != null)
				{
					// get the path of the marking file if one exists
					Address = IPAddress.Parse(Helper.GetAttributeValue(node, "Address", Address.ToString()));

					// port
					Port = Helper.GetAttributeValue(node, "Port", Port);

					foreach (XmlNode @namespaceNode in node.SelectNodes("Namespace"))
					{						
						NamespaceInstance @namespace = NamespaceInstance.Load(@namespaceNode);
						
						Namespaces.Add(@namespace.Address, @namespace); 
					}
				}
			}
			catch (Exception ex)
			{
				// somthing went wrong, tell the user
				MessageBox.Show(ex.Message, "Could not load console setup");
			}
		}

		public void Save()
		{
			try
			{
				XmlDocument doc = new XmlDocument();

				XmlElement node = Helper.CreateElement(doc, "ConsoleSetup");

				Helper.AppendAttributeAndValue(node, "Address", Address.ToString());
				Helper.AppendAttributeAndValue(node, "Port", Port);

				foreach (KeyValuePair<string, NamespaceInstance> keyValue in Namespaces)
				{
					XmlElement @namespace = Helper.CreateElement(doc, "Namespace");

					keyValue.Value.Save(@namespace); 

					node.AppendChild(@namespace); 
				}

				doc.AppendChild(node);

				Helper.EnsurePathExists(Helper.ResolvePath(FilePath));
				doc.Save(Helper.ResolvePath(FilePath));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Could not save console setup");
			}
		}
	}
}
