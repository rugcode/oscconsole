﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace OscConsole.Core
{
	public static class PluginLoader
	{
		public static void LoadPlugin(string path)
		{
			if (File.Exists(path) == false) 
			{
				throw new Exception(""); 
			}

			Assembly asm = Assembly.LoadFile(path);

			ScanAssembly(asm); 
		}

		public static void ScanAssembly(Assembly asm)
		{
			foreach (Type type in asm.GetTypes())
			{
				if (type.IsSubclassOf(typeof(IConsolePlugin)))
				{
					
				}
			}
		} 
	}
}
