﻿using System.Collections.Generic;
using Rug.Cmd;
using Rug.Osc;

namespace OscConsole.Core
{
	public static class StaticObjects
	{
		private static IOscConsoleHost m_OscConsoleHost;

		public static IOscConsoleHost OscConsoleHost 
		{ 
			get { return m_OscConsoleHost; } 
			internal set { m_OscConsoleHost = value; } 
		} 

		public static readonly Dictionary<string, NamespaceInstance> NamespaceLookup = new Dictionary<string, NamespaceInstance>();
		
		public static readonly OscNamespaceManager Manager = new OscNamespaceManager();

		public static readonly Dictionary<string, IMessageConsumer> Consumers = new Dictionary<string, IMessageConsumer>(); 
		
		public static readonly ConsoleThemeColor[] Colors = new ConsoleThemeColor[] 
		{ 
			ConsoleThemeColor.TitleText, 
			ConsoleThemeColor.Text, 
			ConsoleThemeColor.SubText, 
			
			ConsoleThemeColor.TitleText1, 
			ConsoleThemeColor.Text1, 
			ConsoleThemeColor.SubText1,
 			
			ConsoleThemeColor.TitleText2, 
			ConsoleThemeColor.Text2, 
			ConsoleThemeColor.SubText2,
 			
			ConsoleThemeColor.TitleText3, 
			ConsoleThemeColor.Text3, 
			ConsoleThemeColor.SubText3, 

			ConsoleThemeColor.TextGood, 
			ConsoleThemeColor.SubTextGood, 

			ConsoleThemeColor.TextNutral, 
			ConsoleThemeColor.SubTextNutral, 

			ConsoleThemeColor.TextBad, 
			ConsoleThemeColor.SubTextBad, 

			ConsoleThemeColor.PromptColor1, 
			ConsoleThemeColor.PromptColor2,
 
			ConsoleThemeColor.WarningColor1,
 			ConsoleThemeColor.WarningColor2,
		};

	}
}
