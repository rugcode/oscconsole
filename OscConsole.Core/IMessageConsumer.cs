﻿using System;
using Rug.Osc;

namespace OscConsole.Core
{
	public interface IMessageConsumer
	{
		event EventHandler SaveNeeded; 

		/// <summary>
		/// The name of the consumer
		/// </summary>
		string Name { get; } 

		/// <summary>
		/// Attach a namespace instance to the consumer
		/// </summary>
		/// <param name="instance"></param>
		void Attach(NamespaceInstance instance);

		/// <summary>
		/// Detach a namespace instance from the consumer
		/// </summary>
		/// <param name="instance"></param>
		void Detach(NamespaceInstance instance);

		/// <summary>
		/// On the reciving a message this method will be called
		/// </summary>
		/// <param name="instance">The instance that originated the message</param>
		/// <param name="message">A message</param>
		void OnMessage(NamespaceInstance instance, OscMessage message);

		void Save(System.Xml.XmlElement node, NamespaceInstance instance);

		void Load(System.Xml.XmlNode node, NamespaceInstance instance);
	}
}
