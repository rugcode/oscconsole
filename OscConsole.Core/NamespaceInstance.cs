﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using Rug.Cmd;
using Rug.Osc;

namespace OscConsole.Core
{
	public sealed class NamespaceInstance 
	{
		public bool HasRow { get { return Row != null; } }

		public readonly string Address;
		public readonly DataGridViewRow Row; 		
	
		private int m_Count; 
		private int m_LastCount; 
		private double m_Average;
		private bool m_IsMonitoring;

		private int m_ColorIndex;
		private ConsoleThemeColor m_Color; 
		
		public bool IsMonitoring 
		{
			get { return m_IsMonitoring; }
			set
			{
				m_IsMonitoring = value;
				
				UpdateRow(); 
			}
		} 

		public int Count { get { return m_Count; } } 

		public double Average { get { return m_Average; } }

		public int ColorIndex
		{
			get
			{
				return m_ColorIndex; 
			}
			set
			{
				m_ColorIndex = value % StaticObjects.Colors.Length;

				UpdateRow(); 
			}
		}

		public ConsoleThemeColor Color { get { return StaticObjects.Colors[m_ColorIndex]; } }

		public readonly List<IMessageConsumer> Consumers = new List<IMessageConsumer>();

		public readonly List<XmlNode> ConsumersLoadInfo = new List<XmlNode>(); 

		public NamespaceInstance(string address, DataGridViewRow row)
		{
			Address = address; 
			Row = row; 

			IsMonitoring = true;
			ColorIndex = 0; 
		}

		public NamespaceInstance(NamespaceInstance @loadedInstance, DataGridViewRow row)
		{
			Address = @loadedInstance.Address; 
			Row = row;

			IsMonitoring = @loadedInstance.IsMonitoring;
			ColorIndex = @loadedInstance.ColorIndex;
		}

		public void LoadConsumers(NamespaceInstance @loadedInstance)
		{
			foreach (XmlNode node in @loadedInstance.ConsumersLoadInfo)
			{
				string consumer = Helper.GetAttributeValue(node, "type", null);

				if (Helper.IsNullOrEmpty(consumer) == true)
				{
					continue;
				}

				IMessageConsumer messageConsumer;

				if (StaticObjects.Consumers.TryGetValue(consumer, out messageConsumer) == false)
				{
					continue;
				}

				messageConsumer.Load(node, this);
			}
		}

		public void CalculateAverage(TimeSpan time) 
		{
			int diff = m_Count - m_LastCount; 

			m_Average = diff / time.TotalSeconds;

			m_LastCount = m_Count; 

			UpdateRow(); 
		}	

		public void ResetCount() 
		{
			m_LastCount = 0; 
			m_Count = 0; 
			m_Average = 0; 

			UpdateRow(); 
		}

		public void OnMessage(OscMessage message)
		{
			m_Count++;

			if (IsMonitoring == true)
			{
				//if (m_Receiver != null && m_Receiver.State == OscSocketState.Connected)
				{
					RC.WriteLine(Color, message.ToString());
				}
			}

			UpdateRow();

			foreach (IMessageConsumer consumer in Consumers)
			{
				consumer.OnMessage(this, message); 
			}
		}

		private void UpdateRow()
		{
			if (HasRow == true)
			{
				Row.SetValues(Address, Color, Count, Average, IsMonitoring);
			}
		}

		public void Save(System.Xml.XmlElement @namespace)
		{
			Helper.AppendAttributeAndValue(@namespace, "Address", Address);
			Helper.AppendAttributeAndValue(@namespace, "IsMonitoring", IsMonitoring);
			Helper.AppendAttributeAndValue(@namespace, "ColorIndex", ColorIndex);
			
			foreach (IMessageConsumer consumer in Consumers)
			{
				XmlElement consumerNode = Helper.CreateElement(@namespace, "Consumer");

				consumer.Save(consumerNode, this);

				@namespace.AppendChild(consumerNode); 						
			}
		}

		public static NamespaceInstance Load(System.Xml.XmlNode namespaceNode)
		{
			string address = Helper.GetAttributeValue(namespaceNode, "Address", String.Empty);

			if (Helper.IsNullOrEmpty(address) == true)
			{
				throw new Exception("Missing address"); 
			}

			if (OscAddress.IsValidAddressPattern(address) == false)
			{
				throw new Exception("Invalid address '" + address + "'"); 
			}

			NamespaceInstance instance = new NamespaceInstance(address, null);

			instance.IsMonitoring = Helper.GetAttributeValue(namespaceNode, "IsMonitoring", instance.IsMonitoring);
			instance.ColorIndex = Helper.GetAttributeValue(namespaceNode, "ColorIndex", instance.ColorIndex);

			foreach (XmlNode node in namespaceNode.SelectNodes("Consumer"))
			{
				instance.ConsumersLoadInfo.Add(node); 
			}

			return instance; 
		}
	}
}
